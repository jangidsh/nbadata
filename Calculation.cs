﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NBAPOC
{
    public class Calculation
    {
        public int Add(int firstNumber, int secondNumber)
        {
            return firstNumber + secondNumber;
        }

        public int Sub(int firstNumber, int secondNumber)
        {
            return firstNumber - secondNumber;
        }
    }
}
