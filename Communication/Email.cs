﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace NBAPOC.Communication
{
    public static class Email
    {
        public static void SendEmail()
        {
            string to = "ananthkumar.damishetty@thedigitalgroup.com"; //To address    
            string from = "testtdg01@gmail.com"; //From address    
            MailMessage message = new MailMessage(from, to);

            string mailbody = "Test email functionality";
            message.Subject = "Sending Email";
            message.Body = mailbody;
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            //attach report if exists
            string reportName = "LivingDoc.html";   
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, reportName)))
            {
                message.Attachments.Add(new Attachment(Environment.CurrentDirectory + "\\LivingDoc.html"));
            }
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            NetworkCredential basicCredential1 = new
            NetworkCredential("testtdg01@gmail.com", "TestTDG123");
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential1;
            try
            {
                client.Send(message);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
