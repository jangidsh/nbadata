﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NBAPOC
{
    public class Constants
    {
        #region Blob Configuration
        private string _blob_storage_name = "blob-client-id";
        private string _blob_storage_key = "blob-client-secret";
        private string _blob_storage_name_landing = "blob-client-id-landing";
        private string _blob_storage_key_landing = "blob-client-key-landing";

        public string Blob_storage_name
        {
            get
            {
                return _blob_storage_name;
            }
            set
            {
                _blob_storage_name = value;
            }
        }
        public string Blob_storage_key
        {
            get
            {
                return _blob_storage_key;
            }
            set
            {
                _blob_storage_key = value;
            }
        }
        public string Blob_storage_name_landing
        {
            get
            {
                return _blob_storage_name_landing;
            }
            set
            {
                _blob_storage_name_landing = value;
            }
        }
        public string Blob_storage_key_landing
        {
            get
            {
                return _blob_storage_key_landing;
            }
            set
            {
                _blob_storage_key_landing = value;
            }
        }
        #endregion

        #region KeyVault Configuration
        //private string _KV_VAULT_URI = "kv_vault_uri";
        private string _KV_VAULT_URI = "https://kv-dev-egka.vault.azure.net/";
        private string _KV_CLIENT_ID = "kv_client_id";
        private string _KV_CLIENT_SECRET = "kv_client_secret";
        private string _TENANT_ID = "tenant_id";
        private string _SUBSCRIPTION_ID = "subscription_id";

        public string KV_VAULT_URI
        {
            get
            {
                return _KV_VAULT_URI;
            }
            set
            {
                _KV_VAULT_URI = value;
            }
        }
        public string KV_CLIENT_ID
        {
            get
            {
                return _KV_CLIENT_ID;
            }
            set
            {
                _KV_CLIENT_ID = value;
            }
        }
        public string KV_CLIENT_SECRET
        {
            get
            {
                return _KV_CLIENT_SECRET;
            }
            set
            {
                _KV_CLIENT_SECRET = value;
            }
        }
        public string TENANT_ID
        {
            get
            {
                return _TENANT_ID;
            }
            set
            {
                _TENANT_ID = value;
            }
        }
        public string SUBSCRIPTION_ID
        {
            get
            {
                return _SUBSCRIPTION_ID;
            }
            set
            {
                _SUBSCRIPTION_ID = value;
            }
        }
        #endregion

        #region SQLServer configuration
        private string _SQL_SERVER_NAME = "sql-server-name";
        private string _SQL_SERVER_USERNAME = "sql-server-id";
        private string _SQL_SERVER_PASSWORD = "sql-server-key";

        public string SQL_SERVER_NAME
        {
            get
            {
                return _SQL_SERVER_NAME;
            }
            set
            {
                _SQL_SERVER_NAME = value;
            }
        }
        public string SQL_SERVER_USERNAME
        {
            get
            {
                return _SQL_SERVER_USERNAME;
            }
            set
            {
                _SQL_SERVER_USERNAME = value;
            }
        }

        public string SQL_SERVER_PASSWORD
        {
            get
            {
                return _SQL_SERVER_PASSWORD;
            }
            set
            {
                _SQL_SERVER_PASSWORD = value;
            }
        }

        #endregion

        #region ADF Configuration
        private string _ADF_FACTORY_NAME = "adf_factory_name";
        private string _ADF_CLIENT_ID = "adf_client_id";
        private string _ADF_CLIENT_SECRET = "adf_client_secret";

        public string ADF_FACTORY_NAME
        {
            get
            {
                return _ADF_FACTORY_NAME;
            }
            set
            {
                _ADF_FACTORY_NAME = value;
            }
        }
        public string ADF_CLIENT_ID
        {
            get
            {
                return _ADF_CLIENT_ID;
            }
            set
            {
                _ADF_CLIENT_ID = value;
            }
        }
        public string ADF_CLIENT_SECRET
        {
            get
            {
                return _ADF_CLIENT_SECRET;
            }
            set
            {
                _ADF_CLIENT_SECRET = value;
            }
        }
        #endregion
    }
}
