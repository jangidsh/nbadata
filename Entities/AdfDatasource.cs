﻿using Microsoft.Azure.Management.DataFactory;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace NBAPOC.Entities
{
    public class AdfDatasource 
    {
        private static string sportshubTimestamp = "";
        private static string pipelineLogMessgae = "";
        private static string pipelineLogDatasetName = "";
        private static string pipelineRunId = "";
        public async Task RunPipeline(string dataFactoryName,string pipelineName)
        {
            try
            {
                var context = new AuthenticationContext("https://login.windows.net/" + "e898ff4a-4b69-45ee-a3ae-1cd6f239feb2");
                var cc = new ClientCredential("f235c989-c8c9-4a6a-b2e8-201ea367f651", "PJ7Yhv1w00_YKg.FVp4-ttFiQK21i.ys-9");

                var authResult = await context.AcquireTokenAsync("https://management.azure.com/", cc);

                //prepare ADF client
                var cred = new TokenCredentials(authResult.AccessToken);
                //using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = objConstants.SUBSCRIPTION_ID })
                using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = "a20d1d32-3721-4fc6-983a-d0bdb9c5bce9" })
                {
                    var adfName = dataFactoryName;
                    var rgName = "rg-dev-nbait-fandata";
                    //run pipeline
                    var response = await adfClient.Pipelines.CreateRunWithHttpMessagesAsync(rgName, adfName, pipelineName);
                    pipelineRunId = response.Body.RunId;

                    // wait for pipeline to finish
                    var run = await adfClient.PipelineRuns.GetAsync(rgName, adfName, pipelineRunId);
                    while (run.Status == "Queued" || run.Status == "InProgress" || run.Status == "Cancelling")
                    {
                        Thread.Sleep(10000);
                        run = await adfClient.PipelineRuns.GetAsync(rgName, adfName, pipelineRunId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> RetrieveTimestamp(string dataFactory, string dataset)
        {
            try
            {
                var context = new AuthenticationContext("https://login.windows.net/" + "e898ff4a-4b69-45ee-a3ae-1cd6f239feb2");
                var cc = new ClientCredential("f235c989-c8c9-4a6a-b2e8-201ea367f651", "PJ7Yhv1w00_YKg.FVp4-ttFiQK21i.ys-9");

                var authResult = await context.AcquireTokenAsync("https://management.azure.com/", cc);

                //prepare ADF client
                var cred = new TokenCredentials(authResult.AccessToken);
                //using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = objConstants.SUBSCRIPTION_ID })
                using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = "a20d1d32-3721-4fc6-983a-d0bdb9c5bce9" })
                {
                    var rgName = "rg-dev-nbait-fandata";
                    //connect to dataset
                    var response = await adfClient.Datasets.GetAsync(rgName, dataFactory, dataset);

                    var filename = ((Microsoft.Azure.Management.DataFactory.Models.DelimitedTextDataset)response.Properties).Location.FileName.ToString();
                    var folderPath = ((Microsoft.Azure.Management.DataFactory.Models.DelimitedTextDataset)response.Properties).Location.FolderPath.ToString();
                    var filesystem = ((Microsoft.Azure.Management.DataFactory.Models.AzureBlobFSLocation)((Microsoft.Azure.Management.DataFactory.Models.DelimitedTextDataset)response.Properties).Location).FileSystem.ToString();
                    var fullfilepath = filesystem + "/" + folderPath + "/" + filename;
                    var containerName = filesystem + "/" + folderPath;

                    BlobStorageDataSource objBlob = new BlobStorageDataSource();
                    sportshubTimestamp = objBlob.GetBlobFromStorage(filename, containerName);
                    return sportshubTimestamp;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool CompareDates(string compareData)
        {
            try
            {
                bool isDatesAreEqual = false;
                if (compareData == "today")
                {
                    DateTime todays_date = DateTime.Today;
                    string todays_date_month = todays_date.Month.ToString();
                    string todays_date_day = todays_date.Day.ToString();
                    if (Convert.ToInt32(todays_date_month) < 10)
                    {
                        todays_date_month = '0' + todays_date_month;
                    }
                    if (Convert.ToInt32(todays_date_day) < 10)
                    {
                        todays_date_day = '0' + todays_date_day;
                    }
                    string date = Convert.ToString(todays_date.Year) + "-" + todays_date_month + "-" + todays_date_day;
                    isDatesAreEqual = sportshubTimestamp.Contains(date);
                }
                return isDatesAreEqual;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task GetPipelineLogMessage(string pipelineMessage,string pipelineName)
        {
            try
            {
                var context = new AuthenticationContext("https://login.windows.net/" + "e898ff4a-4b69-45ee-a3ae-1cd6f239feb2");
                var cc = new ClientCredential("f235c989-c8c9-4a6a-b2e8-201ea367f651", "PJ7Yhv1w00_YKg.FVp4-ttFiQK21i.ys-9");

                var authResult = await context.AcquireTokenAsync("https://management.azure.com/", cc);

                //prepare ADF client
                var cred = new TokenCredentials(authResult.AccessToken);
                //using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = objConstants.SUBSCRIPTION_ID })
                using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = "a20d1d32-3721-4fc6-983a-d0bdb9c5bce9" })
                {
                    var adfName = "dfdevegka";
                    var rgName = "rg-dev-nbait-fandata";
                    var response = await adfClient.Pipelines.GetAsync(rgName, adfName,pipelineName);
                    for (int i = 0; i < response.Activities.Count; i++)
                    {
                        if (pipelineMessage == "pipeline_success")
                        {
                            if (response.Activities[i].Name == "write_pipeline_success_log_to_json")
                            {
                                foreach (var item in response.Activities[i].AdditionalProperties.Values)
                                {
                                    JObject json = JObject.Parse(item.ToString());
                                    int k;
                                    int count = ((JContainer)json["source"]["additionalColumns"]).Count;
                                    for (k = 0; k < count; k++)
                                    {
                                        string str = ((JProperty)((JContainer)json["source"]["additionalColumns"])[k].First).Value.ToString();
                                        if (str.ToLower().Contains("log"))
                                        {
                                            string logMessgae = ((JProperty)((JContainer)json["source"]["additionalColumns"])[k].First.Next).Value.ToString();
                                            logMessgae.Split("'");
                                            for(int j = 0; j < logMessgae.Split("'").Length; j++)
                                            {
                                                if (logMessgae.Split("'")[j].Contains("SportsHub"))
                                                {
                                                    pipelineLogMessgae = logMessgae.Split("'")[j];
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                //get data set name
                                pipelineLogDatasetName = ((Microsoft.Azure.Management.DataFactory.Models.CopyActivity)response.Activities[i]).Outputs[0].ReferenceName;
                            }
                        }
                        if (pipelineMessage == "file_validation")
                        {
                            if (response.Activities[i].Name == "write_validation_false_log_to_json")
                            {
                                foreach (var item in response.Activities[i].AdditionalProperties.Values)
                                {
                                    JObject json = JObject.Parse(item.ToString());
                                    int k;
                                    int count = ((JContainer)json["source"]["additionalColumns"]).Count;
                                    for (k = 0; k < count; k++)
                                    {
                                        string str = ((JProperty)((JContainer)json["source"]["additionalColumns"])[k].First).Value.ToString();
                                        if (str.ToLower().Contains("log"))
                                        {
                                            pipelineLogMessgae = ((JProperty)((JContainer)json["source"]["additionalColumns"])[k].First.Next).Value.ToString();
                                            break;
                                        }
                                    }
                                }
                                pipelineLogDatasetName = ((Microsoft.Azure.Management.DataFactory.Models.CopyActivity)response.Activities[i]).Outputs[0].ReferenceName;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<bool> ValidateLogMessage()
        {
            try
            {
                bool isLogValidated=false;
                var context = new AuthenticationContext("https://login.windows.net/" + "e898ff4a-4b69-45ee-a3ae-1cd6f239feb2");
                var cc = new ClientCredential("f235c989-c8c9-4a6a-b2e8-201ea367f651", "PJ7Yhv1w00_YKg.FVp4-ttFiQK21i.ys-9");
                var authResult = await context.AcquireTokenAsync("https://management.azure.com/", cc);
                //prepare ADF client
                var cred = new TokenCredentials(authResult.AccessToken);
                using (var adfClient = new DataFactoryManagementClient(cred) { SubscriptionId = "a20d1d32-3721-4fc6-983a-d0bdb9c5bce9" })
                {
                    var adfName = "dfdevegka";
                    var rgName = "rg-dev-nbait-fandata";
                    //connect to dataset
                    var response = await adfClient.Datasets.GetAsync(rgName, adfName, pipelineLogDatasetName);

                    var folderPath = ((Microsoft.Azure.Management.DataFactory.Models.JsonDataset)response.Properties).Location.FolderPath.ToString();
                    var filesystem = ((Microsoft.Azure.Management.DataFactory.Models.AzureBlobFSLocation)((Microsoft.Azure.Management.DataFactory.Models.JsonDataset)response.Properties).Location).FileSystem.ToString();
                    //var directory = filesystem + "/" + folderPath;
                    BlobStorageDataSource objBlob = new BlobStorageDataSource();
                    isLogValidated = objBlob.LogValidation(filesystem,folderPath, pipelineRunId, pipelineLogMessgae).Result;


                }
                return isLogValidated;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

}
