﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace NBAPOC.Entities
{
    public static class AfterFeatures
    {
        //To generate test execution report
        public static void GenerateTestReport()
        {
            var p = new Process
            {
                StartInfo =
                 {
                     FileName = @"C:\\Windows\\system32\\cmd.exe",
                     WorkingDirectory = Environment.CurrentDirectory,
                     Arguments = "/C livingdoc test-assembly NBAPOC.dll -t TestExecution.json"
                 }
            }.Start();

        }


        public static void DeleteTestData(DataTable dt)
        {
            SqlDatasource adf = new SqlDatasource();
            if (dt.Rows.Count > 0)
            {
                adf.DeleteData(dt);
            }
        }
    }
}
