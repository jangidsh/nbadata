﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Parquet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NBAPOC.Entities
{
    public class BlobStorageDataSource : KeyVaultHelper
    {
        private static string accountName = "";
        private static string accessKey = "";
        private static string conn_str = "";
        private static string _containerName = "";
        private static string fileContents = "";

        public BlobStorageDataSource()
        {
        }
        private string GetStorageConnectionString(string containerName)
        {
            Constants objConstants = new Constants();
            if (containerName.Contains("landing"))
            {
                accountName = GetSecretValueofKey(objConstants.Blob_storage_name_landing);
                accessKey = GetSecretValueofKey(objConstants.Blob_storage_key_landing);
                containerName = "landing";
            }
            else
            {
                accountName = GetSecretValueofKey(objConstants.Blob_storage_name);
                accessKey = GetSecretValueofKey(objConstants.Blob_storage_key);
            }
            conn_str = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", accountName, accessKey);
            return conn_str;
        }

        public bool ConnectToContainer(string containerName)
        {
            try
            {
                bool IsContainerConnected = false;
                conn_str = GetStorageConnectionString(containerName);

                var cloudStorageAccount = CloudStorageAccount.Parse(conn_str);
                var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

                var objContainer = cloudBlobClient.GetContainerReference(containerName);
                if (objContainer != null || objContainer.ToString() == "")
                {
                    IsContainerConnected = true;
                    _containerName = containerName;
                }
                return IsContainerConnected;
            }
            catch (Exception ex)
            { 
                throw ex;
            }

        }

        public void DeleteFilesFromContainer(string containerName)
        {
            try
            {
                GetStorageConnectionString(containerName);
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
                CloudBlobClient _blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer _blobContainer = _blobClient.GetContainerReference($"{containerName}");
                BlobContinuationToken blobContinuationToken = null;
                string prefix = "";
                if (containerName.Contains("landing-zone"))
                {
                    prefix = containerName.Replace("landing-zone/", "");
                }
                else { prefix = containerName.Replace("raw-zone/", ""); }
                var blobs = _blobClient.GetContainerReference($"{containerName.Split("/").First()}").ListBlobsSegmentedAsync
                        (prefix, true, BlobListingDetails.All, int.MaxValue, blobContinuationToken, null, null);
                var result = blobs.Result;
                blobContinuationToken = result.ContinuationToken;
                var blobs1 = result.Results.ToList();
                for (int i = 0; i < blobs1.Count; i++)
                {
                    string blobname = ((CloudBlob)blobs1[i]).Name.Split("/").Last();
                    DeleteBlob(blobname, containerName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UploadFileToContainer(string fileName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference(_containerName);
                //container.CreateIfNotExists();
                CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlockBlob myBlob = container.GetBlockBlobReference(fileName);

                var config = new ConfigurationBuilder().AddJsonFile("Config.json").Build();
                string filepath = config["fileUploadPath"];
                var myFileUpload = myBlob.UploadFromFileAsync(filepath + fileName);

                while (Convert.ToString(myFileUpload.Status) == "WaitingForActivation")
                {
                    Thread.Sleep(5000);
                }
                return myFileUpload.IsCompleted;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool ValidateFile(string fileName, string containerName)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlockBlob myBlob = container.GetBlockBlobReference(fileName);
                //File Validation
                var fileExists = myBlob.ExistsAsync();
                return fileExists.Result;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public string GetBlobFromStorage(string fileName, string containerName)
        {
            GetStorageConnectionString(containerName);
            // Setup the connection to the storage account
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);

            // Connect to the blob storage
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient serviceClient = storageAccount.CreateCloudBlobClient();
            // Connect to the blob container
            CloudBlobContainer container = serviceClient.GetContainerReference($"{containerName}");
            // Connect to the blob file
            CloudBlockBlob blob = container.GetBlockBlobReference($"{fileName}");
            // Get the blob file as text
            string result = blob.DownloadTextAsync().Result;
            return result;
        }

        public async Task<bool> LogValidation(string containerName,string prefix, string pipelineRunId,string pipelineLogMessgae)
        {
            try
            {
                GetStorageConnectionString(containerName);
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
                CloudBlobClient _blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer _blobContainer = _blobClient.GetContainerReference($"{containerName}");
                BlobContinuationToken blobContinuationToken = null;
                
                var blobs = _blobClient.GetContainerReference($"{containerName}").ListBlobsSegmentedAsync
                    (prefix, true, BlobListingDetails.All, int.MaxValue, blobContinuationToken, null, null);

                var result = blobs.Result;
                blobContinuationToken = result.ContinuationToken;
                var blobs1 = result.Results.ToList();
                string blobData = "";
                bool logValidation = false;
                for (int i = 0; i < blobs1.Count; i++)
                {
                    if (blobs1[i].GetType() == typeof(CloudBlockBlob))
                    {
                        if (((CloudBlob)blobs1[i]).Name.Split(".").Last().ToString() == "json")
                        {
                            string blobname = ((CloudBlob)blobs1[i]).Name.Split("/").Last();
                            string pathtillFile = ((CloudBlob)blobs1[i]).Name;
                            blobData = GetBlobFromStorage(blobname, containerName + "/" + prefix);
                            
                            if (blobData.Contains(pipelineRunId))
                            {
                                //check log message 
                                if (blobData.ToLower().Contains(pipelineLogMessgae.ToLower()))
                                {
                                    logValidation = true;
                                }
                                else
                                {
                                    logValidation = false;
                                }
                            }
                        }
                    }
                }
                return logValidation;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void DeleteBlob(string blobname, string containerName)
        {
            GetStorageConnectionString(containerName);
            // Setup the connection to the storage account
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);

            // Connect to the blob storage
            CloudBlobClient serviceClient = storageAccount.CreateCloudBlobClient();
            // Connect to the blob container
            CloudBlobContainer container = serviceClient.GetContainerReference($"{containerName}");
            // Connect to the blob file
            CloudBlockBlob blob = container.GetBlockBlobReference($"{blobname}");
            // Delete the blob file as text
            blob.DeleteIfExistsAsync();
        }

        public async Task DeleteAllBlobs(string containerName)
        {
            GetStorageConnectionString(containerName);
            //string containerName = "landing-zone/pipeline_configs/landing_raw/logs/sportshub";
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
            CloudBlobClient _blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer _blobContainer = _blobClient.GetContainerReference($"{containerName}");
            BlobContinuationToken blobContinuationToken = null;
            string prefix = "";
            if (containerName.Contains("landing-zone"))
            {
                prefix = containerName.Replace("landing-zone/", "");
            }
            else { prefix = containerName.Replace("raw-zone/", ""); }
            var blobs = _blobClient.GetContainerReference($"{containerName.Split("/").First()}").ListBlobsSegmentedAsync
                    (prefix, true, BlobListingDetails.All, int.MaxValue, blobContinuationToken, null, null);

            var result = blobs.Result;
            blobContinuationToken = result.ContinuationToken;
            var blobs1 = result.Results.ToList();
            for (int i = 0; i < blobs1.Count; i++)
            {
                string blobname = ((CloudBlob)blobs1[i]).Name.Split("/").Last();
                DeleteBlob(blobname, containerName);
            }
        }

        public void Readfile(string filename)
        {
            GetStorageConnectionString(_containerName);
            // Setup the connection to the storage account
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(_containerName);

            CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
            CloudBlockBlob myBlob = container.GetBlockBlobReference(filename);
            //Reading contents from file
            //string fileContents = myBlob.DownloadTextAsync().Result;
            //Download csv file to local
            var config = new ConfigurationBuilder().AddJsonFile("Config.json").Build();
            string filepath = config["fileDownloadPath"] + filename;
            myBlob.DownloadToFileAsync(filepath, FileMode.Create);
            //Delete file from raw zone (as part of POC deleting test file)
            myBlob.DeleteIfExistsAsync();
        }

        public string[] ReadFanInteractionIDsFromParquet(List<string> Prefixes)
        {
            GetStorageConnectionString(_containerName);
            string[] FanInteractionIds = new string[] { };
            foreach (string prefix in Prefixes)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(_containerName + prefix);
                //CloudBlobContainer container = blobClient.GetContainerReference(_containerName + "/partition_year=2021/partition_month=9/"); //+ "/partition_year=2021/partition_month=9/"
                CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlockBlob myBlob;
                BlobContinuationToken blobContinuationToken = null;
                List<Uri> allBlobs = new List<Uri>();
                string prefix1 = _containerName.Split("/").Last().ToString() + prefix;
                //string prefix = "identity/partition_year=2021/partition_month=9/";//identity/partition_year=2021/partition_month=9/
                var blobs = myBlobClient.GetContainerReference($"{_containerName.Split("/").First().ToString()}").ListBlobsSegmentedAsync
                    (prefix1, true, BlobListingDetails.All, int.MaxValue, blobContinuationToken, null, null);

                var result = blobs.Result;
                blobContinuationToken = result.ContinuationToken;
                var blobs1 = result.Results.ToList();
                
                for (int i = 0; i < blobs1.Count; i++)
                {
                    if (blobs1[i].GetType() == typeof(CloudBlockBlob))
                    {
                        if (((CloudBlob)blobs1[i]).Name.Split(".").Last().ToString() == "parquet")
                        {
                            string blobname = ((CloudBlob)blobs1[i]).Name.Split("/").Last();
                            myBlob = container.GetBlockBlobReference(blobname);
                            using (Stream fileStream = myBlob.OpenReadAsync().Result)
                            {
                                using (var parquetReader = new ParquetReader(fileStream))
                                {
                                    List<string> ls = FanInteractionIds.ToList();
                                    // get file schema (available straight after opening parquet reader)
                                    // however, get only data fields as only they contain data values
                                    Parquet.Data.DataField[] dataFields = parquetReader.Schema.GetDataFields();

                                    // enumerate through row groups in this file
                                    for (int j = 0; j < parquetReader.RowGroupCount; j++)
                                    {
                                        // create row group reader
                                        using (ParquetRowGroupReader groupReader = parquetReader.OpenRowGroupReader(j))
                                        {
                                            // read all columns inside each row group 
                                            Parquet.Data.DataColumn[] columns = dataFields.Select(groupReader.ReadColumn).ToArray();
                                            // get fanInteractionID column
                                            int index = -1;
                                            for (int p = 0; p < columns.Length; p++)
                                            {
                                                // get column index
                                                if (columns[p].Field.Name == "fan_interaction_id")
                                                {
                                                    index = p;
                                                }
                                            }
                                            if (index >= 0)
                                            {
                                                Parquet.Data.DataColumn fanInteractionIDColumn = columns[index];
                                                //Parquet.Data.DataColumn firstColumn = columns[13];
                                                // .Data member contains a typed array of column data you can cast to the type of the column
                                                Array data = fanInteractionIDColumn.Data;
                                                //FanInteractionIds = (string[])data;
                                                //FanInteractionIds.Append((string[])data);
                                                for(int b=0; b < data.Length; b++)
                                                {
                                                    ls.Add(((string[])data)[b]);
                                                }
                                                FanInteractionIds = ls.ToArray();
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                
            }
            //after your loop
            return FanInteractionIds;
        }

        public bool ValidateFolders(List<string> Prefixes)
        {
            bool isFolderExists = false;
            foreach(string prefix in Prefixes)
            {
                GetStorageConnectionString(_containerName);
                //string filename = "part-00011-7cb55db2-5a60-4260-b9ac-bb7b2a5165c5.c000.snappy.parquet";
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference(_containerName + prefix); //+ "/partition_year=2021/partition_month=9/"
                CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
                CloudBlockBlob myBlob;
                BlobContinuationToken blobContinuationToken = null;
                string prefix1 = _containerName.Split("/").Last().ToString() + prefix;
                
                var blobs = myBlobClient.GetContainerReference($"{_containerName.Split("/").First().ToString()}").ListBlobsSegmentedAsync
                (prefix1, true, BlobListingDetails.All, int.MaxValue, blobContinuationToken, null, null);

                var result = blobs.Result;
                blobContinuationToken = result.ContinuationToken;
                var blobs1 = result.Results.ToList();
                if (blobs1.Count > 0)
                {
                    isFolderExists = true;
                }
            }
            return isFolderExists;
        }
    }
}
