﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using TestProject.Helper;

namespace NBAPOC.Entities
{
    public class Helper
    {
        public bool ValidateColumn(string[] FanInteractionIDs, DataTable dtFanInteractionIds)
        {
            bool contains=false;

            //Check processed file FanInteractionIDs in datbase
            for (int i = 0; i < FanInteractionIDs.Length; i++)
            {
                contains = dtFanInteractionIds.AsEnumerable().Any(row => FanInteractionIDs[i] == row.Field<String>("fan_interaction_id"));
                if (!contains)
                {
                    break;
                }
            }
            return contains;
        }

        public static List<string> PrefixList = new List<string>();

        public List<string> getPrefixListFromCSV(string fileName)
        {
            DataTable dtCsv = new DataTable();
            var config = new ConfigurationBuilder().AddJsonFile("Config.json").Build();
            string filePath = config["fileUploadPath"] + fileName;
            string Fulltext = "";
            using (StreamReader sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j]); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString();

                                }
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }
            var activityDates = dtCsv.AsEnumerable().Select(r => r.Field<string>("R_ACTIVITY_DATE").Split(" ").FirstOrDefault()).ToList();
            //convering string activitydates to List<Datetime>
            string year, month, day;
            var dates = new List<DateTime>();
            foreach (string item in activityDates)
            {
                string[] values = item.Split("/");
                day = values[0];
                month = values[1];
                year = values[2];
                string dtt = day + "/" + month + "/" + year;
                dates.Add(DateTime.ParseExact(dtt, "M/d/yyyy", null));

            }
            //Get Distinct Month/Year
            List<DateTime> result = dates
                  .Select(d => new DateTime(d.Year, d.Month, 1))
                  .Distinct()
                  .ToList();
            //Prepare Prefix list
            //var prefixes = new List<string>();
            PrefixList.Clear();
            foreach (DateTime d in result)
            {
                if (d.Month < 10)
                {
                    //Appending zero
                    PrefixList.Add("/partition_year=" + d.Year + "/partition_month=0" + d.Month + "/");
                }
                else
                {
                    PrefixList.Add("/partition_year=" + d.Year + "/partition_month=" + d.Month + "/");
                }
            }
            return PrefixList;
        }

        public List<string> getPrefixListFromJSON(string fileName)
        {
            DataTable dtCsv = new DataTable();
            var config = new ConfigurationBuilder().AddJsonFile("Config.json").Build();
            string filePath = config["fileUploadPath"] + fileName;

            string jsonFromFile = File.ReadAllText(filePath);
            var json = JToken.Parse(jsonFromFile);
            var fieldsCollector = new JsonFieldsCollector(json);
            var fields = fieldsCollector.GetAllFields();

            List<string> lastUpdatedDates = new List<string>();
            foreach (var field in fields)
            {
                if (field.Key.Split(".").Last() == "lastUpdated")
                {
                    lastUpdatedDates.Add(field.Value.Value.ToString().Split(" ").First());
                }
            }
            //convering string lastUpdateDate to List<Datetime>
            string year, month, day;
            var dates = new List<DateTime>();
            foreach (string item in lastUpdatedDates)
            {
                string[] values = item.Split("-");
                year = values[0];
                month = values[1];
                day = values[2];
                string dtt = day + "/" + month + "/" + year;
                dates.Add(DateTime.ParseExact(dtt, "M/d/yyyy", null));

            }
            //Get Distinct Month/Year
            List<DateTime> result = dates
                  .Select(d => new DateTime(d.Year, d.Month, 1))
                  .Distinct()
                  .ToList();
            //Prepare Prefix list
            PrefixList.Clear();
            foreach (DateTime d in result)
            {
                if (d.Month < 10)
                {
                    //Appending zero
                    PrefixList.Add("/partition_year=" + d.Year + "/partition_month=0" + d.Month + "/");
                }
                else
                {
                    PrefixList.Add("/partition_year=" + d.Year + "/partition_month=" + d.Month + "/");
                }
            }
            return PrefixList;
        }

        public StringBuilder getFanInteractionIDs(string[] fanInteractionIDs)
        {
            StringBuilder ids = new StringBuilder();
            foreach(string item in fanInteractionIDs.Distinct())
            {
                ids.Append(item + Environment.NewLine);
            }
            return ids;
        }
        public StringBuilder getColumnNames(List<string> colNames)
        {
            StringBuilder columns = new StringBuilder();
            foreach (string item in colNames)
            {
                columns.Append(item + Environment.NewLine);
            }
            return columns;
        }
        public StringBuilder getFanInteractionIdsFromDB(DataTable dtFanInteractionIds)
        {
            StringBuilder ids = new StringBuilder();
            foreach (DataRow row in dtFanInteractionIds.Rows)
            {
                ids.Append(row["fan_interaction_id"].ToString() + Environment.NewLine);
            }
            return ids;
        }

        public bool CheckItem(List<string> items, string itemToCheck)
        {
            bool isItemExist = false;
            foreach (string str in items)
            {
                if (itemToCheck.Equals(str))
                {
                    isItemExist = true;
                    break;
                }
                else
                {
                    isItemExist = false;
                }
            }
            return isItemExist;
        }
    }
}
