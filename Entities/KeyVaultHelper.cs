﻿using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using System;
using System.Collections.Generic;
using System.Text;

namespace NBAPOC.Entities
{
    public class KeyVaultHelper : Constants
    {
        private readonly SecretClient _keyVaultClient;
       
        public KeyVaultHelper()
        {
            Constants objConstants = new Constants();
            //var kvUrl = "https://kv-dev-egka.vault.azure.net/";
            var kvUrl = objConstants.KV_VAULT_URI;
            if (kvUrl?.Length > 0)
                _keyVaultClient = new SecretClient(new Uri(kvUrl), new DefaultAzureCredential());
        }

        public string GetSecretValueofKey(string key)
        {
            try
            {
                string value = string.Empty;
                value = _keyVaultClient.GetSecret(key).Value.Value;
                return value;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
