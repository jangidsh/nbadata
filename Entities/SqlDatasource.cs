﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace NBAPOC.Entities
{
    public class SqlDatasource : KeyVaultHelper
    {
        private string server_name = "";
        private string server_username = "";
        private string server_password = "";
        private string conn_str = "";
        private string BuildConnectionString(string database_name)
        {
            Constants objConstants = new Constants();
            server_name = GetSecretValueofKey(objConstants.SQL_SERVER_NAME);
            server_username = GetSecretValueofKey(objConstants.SQL_SERVER_USERNAME);
            server_password = GetSecretValueofKey(objConstants.SQL_SERVER_PASSWORD);
            //string driver = "{ODBC Driver 17 for SQL Server}";
            //return "DRIVER=" + driver + ";SERVER=" + server_name + ";PORT=1433;DATABASE=" + database_name + ";UID=" + server_username + ";PWD=" + server_password;
            conn_str = "SERVER=" + server_name + ";DATABASE=" + database_name + ";UID=" + server_username + ";PWD=" + server_password;
            return conn_str;
        }

        public DataTable GetData(string databaseName, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();
                string connString = BuildConnectionString(databaseName);
                string query = "SELECT fan_interaction_id FROM " + tableName +
                    " WHERE CONVERT(VARCHAR(10), etl_created_date, 111)= '" + DateTime.Now.ToString("yyyy-MM-dd") + "' ORDER BY etl_created_date DESC";

                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();

                // create data adapter
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                return dt;
                conn.Close();
                da.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<string> getColumnNames(string databaseName, string tableName)
        {
            List<string> colList = new List<string>();
            string connString = BuildConnectionString(databaseName);
            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText =
                    "SELECT c.name FROM sys.columns c INNER JOIN sys.tables t  on t.object_id = c.object_id AND t.name ='" + tableName + "'  AND t.type = 'U'";
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        colList.Add(reader.GetString(0));
                    }
                }
            }
            return colList;
        }

        //Delete data (As part of POC deleting test data)
        public void DeleteData(DataTable Ids)
        {
            string databaseName = "synsqlpdevmyh";
            string tableName = "schfanprestg.fan_pii_signupdata2";
            try
            {
                string connString = BuildConnectionString(databaseName);
                SqlConnection conn = new SqlConnection(connString);
                foreach (DataRow row in Ids.Rows)
                {
                    string query = "DELETE FROM " + tableName + "WHERE fan_interaction_id='" + row["fan_interaction_id"].ToString() + "'";                    
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
               
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
