﻿Feature: ETL_ADF_EndToEnd
	In order to handle the ETL testing
	As a Test person
	I want to test SignupData ADF Pipelines End-to-End

@FunctionalTest @Demo
Scenario: 01 SignupData2_Verify the schema for synsqlpdevmyh.fan_pii_signupdata2 table 
	Given I have connected to database "synsqlpdevmyh" and fetch all columns for "fan_pii_signupdata2"
	And I validate column "fan_interaction_id" is present in database table
	And I validate column "id_source" is present in database table
	And I validate column "valid_from" is present in database table
	And I validate column "email_address" is present in database table
	And I validate column "first_name" is present in database table
	And I validate column "last_name" is present in database table
	And I validate column "phone_type" is present in database table
	And I validate column "phone_number" is present in database table
	And I validate column "zip_code" is present in database table
	And I validate column "country" is present in database table
	And I validate column "date_of_birth" is present in database table
	And I validate column "etl_created_date" is present in database table

@FunctionalTest @Demo
Scenario: 02 SignupData2_Verify the schema for fan_id_ingest table
	Given I have connected to database "synsqlpdevmyh" and fetch all columns for "fan_id_ingest"
	And I validate column "fan_interaction_id" is present in database table
	And I validate column "id_source" is present in database table
	And I validate column "valid_from" is present in database table
	And I validate column "id_type" is present in database table
	And I validate column "id_value" is present in database table
	And I validate column "etl_created_date" is present in database table	

@RegressionTesting @Demo
Scenario: 03 ADFSignupData2- Verify that data populated into Data warehouse after successful ADF pipeline run
	Given I have connected to "landing-zone/signupdata2" container
	Then I delete files from "landing-zone/signupdata2"
	Given I have connected to "raw-zone/signup/signupdata2/signupdata2" container
	Then I delete files from "raw-zone/signup/signupdata2/signupdata2"
	Then I delete files from "raw-zone/signup/signupdata2/temp_dw"
	Then I delete files from "raw-zone/signup/signupdata2/checkpoints"
	Given I have connected to "landing-zone/signupdata2" container
	Then I upload a valid "MSFT-signupdata2.csv" file
	And I have connected to azure data factory "dfdevegka" and run pipeline "df-signupdata2-landing-raw"
	And I have connected to "raw-zone/signup/signupdata2/signupdata2" container
	And I validate file "MSFT-signupdata2.csv" exists in location "raw-zone/signup/signupdata2/signupdata2"
	And I connect to datafactory "dfdevegka" and retrieve timestamp from dataset "ds_read_signupdata2_timestamp"
	Then I have connected to "landing-zone/signupdata2" container
	Then I validate dataset data is equal to "today"
	And I get log message "pipeline_success" for pipeline "df-signupdata2-landing-raw"
	And I validate "log_message"
	Given I have connected to "raw-zone/signup/signupdata2/signupdata2" container
	Then I read file "MSFT-signupdata2.csv"
	Then I have connected to "processed-zone/signup" container
	When I connect to result container and validate folder creation
	Then I have connected to database "synsqlpdevmyh" and fetch data for "schfanprestg.fan_pii_signupdata2"
	And I validate "fan_interaction_id" from table is to equal "fan_interaction_id" from ProcessedFile
	#Then I have connected to database "synsqlpdevmyh" and fetch data for "schfanprestg.fan_id_ingest"
	#And I validate "fan_interaction_id" from table is to equal "fan_interaction_id" from ProcessedFile

#@Test
#Scenario: ADFSignupData2- Delete files from Landing Zone & Raw Zone
#	Given I have connected to "landing-zone/signupdata2" container
#	Then I delete files from "landing-zone/signupdata2"
#	Given I have connected to "raw-zone/signup/signupdata2/signupdata2" container
#	Then I delete files from "raw-zone/signup/signupdata2/signupdata2"
#	Then I delete files from "raw-zone/signup/signupdata2/temp_dw"
#	Then I delete files from "raw-zone/signup/signupdata2/checkpoints"
#
#@Test 
#Scenario: ADFSignupData2- Upload files To Landing Zone 
#	Given I have connected to "landing-zone/signupdata2" container
#	Then I upload a valid "MSFT-signupdata2.csv" file
