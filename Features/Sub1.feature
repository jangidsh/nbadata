﻿Feature: TestSub1
	As a Test person
	I want to test Sub of two numbers

@parallelSubExecution1
Scenario Outline: Add two numbers through scenario outline
	Given I have entered <firstNumber> into the calculator
	And I also have entered <secondNumber> into the calculator
	When I press sub
	Then the result should be <result> on the screen

	Examples:
		| firstNumber | secondNumber | result |
		| 10        | 2          | 8    |
		| 10        | 3          | 7    |
		| 10        | 4          | 6    |