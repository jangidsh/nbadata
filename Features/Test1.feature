﻿Feature: Test1
	Simple calculator for adding two numbers

@ParallelExecution11
Scenario Outline: Add two numbers through scenario outline
	Given I have entered <firstNumber> into the calculator
	And I also have entered <secondNumber> into the calculator
	When I press add
	Then the result should be <result> on the screen
	Examples:
    | firstNumber | secondNumber | result |
    | 100         | 200          | 300    |
	| 100         | 300          | 400    |
	| 100         | 400          | 500    |