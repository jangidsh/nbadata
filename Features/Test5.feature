﻿Feature: Test5
	Simple calculator for adding two numbers

@ParallelExecution
Scenario Outline: Add two numbers through scenario outline
	Given I have entered <firstNumber> into the calculator
	And I also have entered <secondNumber> into the calculator
	When I press add
	Then the result should be <result> on the screen
	Examples:
    | firstNumber | secondNumber | result |
    | 1         | 50         | 51    |
	| 1         | 10         | 11    |
	| 1         | 15         | 16    |