﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TestProject.Helper;

namespace NBAPOC
{
    //To read and capture respective feed table columns in a list.
    //Later will compare this table column list with db table.
    public static class Feeds
    {
        static Feeds()
        {
            ReadDatabricksConfig();
        }
        //Sportshub
        public static List<string> sportshub_table_name_pii = new List<string>();
        public static List<string> sportshub_table_name_id = new List<string>();
        public static List<string> sportshub_table_name_prestaging = new List<string>();
        //CIAM
        public static List<string> ciam_table_name_pii = new List<string>();
        public static List<string> ciam_table_name_id = new List<string>();
        public static List<string> ciam_table_name_prestaging = new List<string>();
        //adhoc
        public static List<string> adhoc_table_name_pii = new List<string>();
        public static List<string> adhoc_table_name_id = new List<string>();
        public static List<string> adhoc_table_name_prestaging = new List<string>();


        private static void ReadDatabricksConfig()
        {
            string filepath = Environment.CurrentDirectory.Replace("\\bin\\Debug\\netcoreapp3.1", "\\") + "databricks_configs.json";
            string jsonFromFile = File.ReadAllText(filepath);
            var json = JToken.Parse(jsonFromFile);
            var fieldsCollector = new JsonFieldsCollector(json);
            var fields = fieldsCollector.GetAllFields();

            foreach (var field in fields)
            {
                if (field.Key.Split(".")[0].Contains("sportshub"))
                {
                    if (field.Key.Contains("sportshub.pii_table"))
                    {
                        sportshub_table_name_pii.Add(field.Value.ToString());
                    }
                    if (field.Key.Contains("sportshub.id"))
                    {

                        sportshub_table_name_id.Add(field.Value.ToString());
                    }
                    if (field.Key.Contains("sportshub.prestaging"))
                    {
                        sportshub_table_name_prestaging.Add(field.Value.ToString());
                    }
                }
                if (field.Key.Split(".")[0].Contains("ciam"))
                {
                    if (field.Key.Contains("ciam.pii_table"))
                    {
                        ciam_table_name_pii.Add(field.Value.ToString());
                    }
                    if (field.Key.Contains("ciam.id_table"))
                    {

                        ciam_table_name_id.Add(field.Value.ToString());
                    }
                    if (field.Key.Contains("ciam.prestaging"))
                    {
                        ciam_table_name_prestaging.Add(field.Value.ToString());
                    }
                }
                if (field.Key.Split(".")[0].Contains("adhoc"))
                {
                    if (field.Key.Contains("adhoc.pii_table"))
                    {
                        adhoc_table_name_pii.Add(field.Value.ToString());
                    }
                    if (field.Key.Contains("adhoc.id"))
                    {

                        adhoc_table_name_id.Add(field.Value.ToString());
                    }
                    if (field.Key.Contains("adhoc.prestaging"))
                    {
                        adhoc_table_name_prestaging.Add(field.Value.ToString());
                    }
                }
            }

        }
    }
}
