﻿//using FanDuelTest;
//using Microsoft.AspNetCore.StaticFiles;
//using Microsoft.WindowsAzure.Storage;
//using Microsoft.WindowsAzure.Storage.Blob;
//using Parquet;
//using Parquet.Data;
//using Parquet.Data.Rows;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace TestProject.Helper
//{
//    public class StorageHelper : SettingsHelper
//    {
//        Constants _blobConstants = new Constants();
//        public bool IsContainerConnected(string containerName)
//        {
//            bool IsContainerConnected = false;
//            //Get Connection string from KeyVault
//            string accountName = GetSetting(_blobConstants.Blob_storage_name);
//            string accessKey = GetSetting(_blobConstants.Blob_storage_key);
//            string conn_str = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", accountName, accessKey);

//            var cloudStorageAccount = CloudStorageAccount.Parse(conn_str);
//            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

//            var objContainer = cloudBlobClient.GetContainerReference(containerName);
//            if (objContainer != null || objContainer.ToString() == "")
//            {
//                IsContainerConnected = true;
//            }
//            return IsContainerConnected;
//        }

//        public bool UploadFileToAzureStorage(string containerName)
//        {
//            //Get Connection string from KeyVault
//            string accountName = GetSetting(_blobConstants.Blob_storage_name);
//            string accessKey = GetSetting(_blobConstants.Blob_storage_key);
//            string conn_str = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", accountName, accessKey);

//            string filename = "MSFT-Braze-mobile_sampleTest_byAnanth.json";

//            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
//            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
//            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
//            //container.CreateIfNotExists();
//            CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
//            CloudBlockBlob myBlob = container.GetBlockBlobReference(filename);
                       
//            var myFileUpload =  myBlob.UploadFromFileAsync(@"C:\Users\adamishetty\Documents\NBA-POC\FnaDuelTest\TestProject\Files\" + filename);
            
//            while(Convert.ToString(myFileUpload.Status)=="WaitingForActivation"){
//                Thread.Sleep(20000);
//            }
//            //File Validation
//            //bool b = myBlob.ExistsAsync().Result;
//            //var afterUpload = myBlob.ExistsAsync();

//            //Reading contents from file
//            //string contents = myBlob.DownloadTextAsync().Result;

//            //File Deletion
//            //myBlob.DeleteIfExistsAsync();

//            return myFileUpload.IsCompleted;
//        }

        
//        public void ReadParqueFile(string containerName)
//        {
//            //Get Connection string from KeyVault
//            string accountName = GetSetting(_blobConstants.Blob_storage_name);
//            string accessKey = GetSetting(_blobConstants.Blob_storage_key);
//            string conn_str = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net", accountName, accessKey);
           
//            string filename = "part-00001-d28a1097-7e79-4b9d-931f-ded27c87ebf4.c000.snappy.parquet";
//            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn_str);
//            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
//            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
//            CloudBlobClient myBlobClient = storageAccount.CreateCloudBlobClient();
//            CloudBlockBlob myBlob = container.GetBlockBlobReference(filename);
            
//            //To read content from Local directory.
//           // using (Stream fileStream = System.IO.File.OpenRead(@"C:\Users\adamishetty\Documents\NBA-POC\FnaDuelTest\TestProject\Files\part-00001-d28a1097-7e79-4b9d-931f-ded27c87ebf4.c000.snappy.parquet"))
            
//            using(Stream fileStream = myBlob.OpenReadAsync().Result )
//            {
//                using (var parquetReader = new ParquetReader(fileStream))
//                {
//                    DataField[] dataFields = parquetReader.Schema.GetDataFields();
//                    Table finalTable = new Table(dataFields);
//                    var reader = new ParquetReader(fileStream);

//                    Table staging = reader.ReadAsTable();
//                    foreach (Row row in staging)
//                    {
//                        finalTable.Add(row);
//                    }
//                    string swPath = @"C:\Users\adamishetty\Documents\NBA-POC\FnaDuelTest\TestProject\Files\SampleFile4.txt";
//                    StreamWriter sw = new StreamWriter(swPath);
//                    StringBuilder sb = new StringBuilder();
//                    for(int i = 0; i < dataFields.Count(); i++)
//                    {
//                        sb.Append(dataFields[i] + ",");
//                    }
//                    sw.WriteLine(sb);
//                    foreach(Row row in finalTable)
//                    {
//                        sw.WriteLine(row);
//                    }
//                }
//            }
           

//        }
    
//        public string GetBlob()
//        {
//            string connectionString = "AzureConnectionString";
//            string fileName = "SampleFile.txt";
//            string containerName = "raw-zone/testing/email/braze/braze_mobile";

//            // Setup the connection to the storage account
//            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);

//            // Connect to the blob storage
//            Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient serviceClient = storageAccount.CreateCloudBlobClient();
//            // Connect to the blob container
//            CloudBlobContainer container = serviceClient.GetContainerReference($"{containerName}");
//            // Connect to the blob file
//            CloudBlockBlob blob = container.GetBlockBlobReference($"{fileName}");
//            // Get the blob file as text
//            string contents = blob.DownloadTextAsync().Result;

//            return contents;
//        }
//    }
//}

