﻿using NBAPOC.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace NBAPOC
{
    [Binding]
    public sealed class Hooks
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks

        [BeforeScenario]
        public void BeforeScenario()
        {
            //TODO: implement logic that has to run before executing each scenario
        }

        [AfterScenario]
        public void AfterScenario()
        {
            //TODO: implement logic that has to run after executing each scenario
        }

        [BeforeTestRun]
        public static void DeleteExistingLivingDocReport()
        {
            try
            {
                string reportName = "LivingDoc.html";
                // Check if file exists with its full path    
                if (File.Exists(Path.Combine(Environment.CurrentDirectory, reportName)))
                {
                    // If file found, delete it    
                    File.Delete(Path.Combine(Environment.CurrentDirectory, reportName));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }

        }

        [AfterTestRun]
        public static void TestAF()
        {
            try
            {
                AfterFeatures.GenerateTestReport();
                //Thread.Sleep(5000);
                //Communication.Email.SendEmail();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }

        }

    }
}
