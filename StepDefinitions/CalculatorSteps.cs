﻿using NUnit.Framework;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Infrastructure;
using NBAPOC.Entities;

[assembly: Parallelizable(ParallelScope.Fixtures)]
[assembly: LevelOfParallelism(10)]
namespace NBAPOC.StepDefinitions
{
    [Binding]
    
    public class CalculatorSteps
    {   
        private ScenarioContext _scenarioContext;
        private readonly ISpecFlowOutputHelper _specFlowOutputHelper;
        public CalculatorSteps(ScenarioContext scenarioContext, ISpecFlowOutputHelper outputHelper)
        {
            _scenarioContext = scenarioContext;
            _specFlowOutputHelper = outputHelper;
        }
              
        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int firstNumber)
        {
            _specFlowOutputHelper.WriteLine("Start TIme: " + DateTime.Now.ToString());
            _scenarioContext.Set(firstNumber, "firstNumber");
        }
        
        [Given(@"I also have entered (.*) into the calculator")]
        public void GivenIAlsoHaveEnteredIntoTheCalculator(int secondNumber)
        {
            _scenarioContext.Set(secondNumber, "secondNumber");
        }
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            var firstNumber = _scenarioContext.Get<int>("firstNumber");
            var secondNumber = _scenarioContext.Get<int>("secondNumber");
            var calculation = new Calculation();
            var add = calculation.Add(firstNumber, secondNumber);
            _scenarioContext.Set(add, "actual");
            Thread.Sleep(2000);
        }
        
        [When(@"I press sub")]
        public void WhenIPressSub()
        {
            var firstNumber = _scenarioContext.Get<int>("firstNumber");
            var secondNumber = _scenarioContext.Get<int>("secondNumber");
            var calculation = new Calculation();
            var add = calculation.Sub(firstNumber, secondNumber);
            _scenarioContext.Set(add, "actual");
            Thread.Sleep(5000);
        }
        
        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int expected)
        {
            var actual = _scenarioContext.Get<int>("actual");
            Assert.AreEqual(expected, actual);
            _specFlowOutputHelper.WriteLine(DateTime.Now.ToString());
        }

        //[Scope(Feature = "Test1")]
        //[AfterFeature]
        //public static void TestAF()
        //{
        //    try
        //    {
        //        AfterFeatures.GenerateTestReport();
        //        Communication.Email.SendEmail();
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //    }
           
        //}
    }
}
