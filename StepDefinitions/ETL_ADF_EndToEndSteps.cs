﻿using NBAPOC.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Infrastructure;
using System.Configuration;
using System.IO;

namespace NBAPOC.StepDefinitions
{
    [Binding]
    public class ETL_ADF_EndToEndSteps
    {
        private readonly ISpecFlowOutputHelper _specFlowOutputHelper;
        private static string sportshubTimestamp = "";
        public static DataTable dtFanInteractionIds;
        private static string LandingZonefileName="";
        private static List<string> colList =new List<string>();

        public ETL_ADF_EndToEndSteps(ISpecFlowOutputHelper outputHelper)
        {
            _specFlowOutputHelper = outputHelper;
        }

        BlobStorageDataSource objBlob = new BlobStorageDataSource();
        AdfDatasource objAdf = new AdfDatasource();
        SqlDatasource objSql = new SqlDatasource();
        Helper _helper = new Helper();

        [Given(@"I have connected to ""(.*)"" container")]
        public void GivenIHaveConnectedToContainer(string containerName)
        {
            bool IsConnectedToContainer = false;
            try
            {
                IsConnectedToContainer = objBlob.ConnectToContainer(containerName);
                if (IsConnectedToContainer)
                {
                    _specFlowOutputHelper.WriteLine("Connected to {0} container", containerName);
                }
                else
                {
                    _specFlowOutputHelper.WriteLine("Conection to {0} container Failed", containerName);
                }
                
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(IsConnectedToContainer, Is.True);
        }

        [Then(@"I delete files from ""(.*)""")]
        public void ThenIDeleteFilesFrom(string containerName)
        {
            try
            {
                objBlob.DeleteFilesFromContainer(containerName);
                //Assert.That(isFileDeleted, Is.True);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }

        }

        [Then(@"I upload a valid ""(.*)"" file")]
        public void ThenIUploadAValidFile(string fileName)
        {
            bool isFileUploaded = false;
            try
            {
                isFileUploaded = objBlob.UploadFileToContainer(fileName);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(isFileUploaded, Is.True);
        }

        [Then(@"I have connected to azure data factory ""(.*)"" and run pipeline ""(.*)""")]
        public async Task ThenIHaveConnectedToAzureDataFactoryAndRunPipeline(string dataFactory, string pipelineName)
        {
            try
            {
                await objAdf.RunPipeline(dataFactory, pipelineName);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }

        }

        [Then(@"I have connected to ""(.*)"" container")]
        public void ThenIHaveConnectedToContainer(string containerName)
        {
            bool IsContainerConnected = false;
            try
            {
                IsContainerConnected = objBlob.ConnectToContainer(containerName);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(IsContainerConnected, Is.True);
        }
        [Then(@"I validate file ""(.*)"" exists in location ""(.*)""")]
        public void ThenIValidateFileExistsInLocation(string fileName, string containerName)
        {
            bool isFileExist = false;
            try
            {
                isFileExist = objBlob.ValidateFile(fileName, containerName);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(isFileExist, Is.True);
        }

        [Then(@"I connect to datafactory ""(.*)"" and retrieve timestamp from dataset ""(.*)""")]
        public async Task ThenIConnectToDatafactoryAndRetrieveTimestampFromDataset(string datafactoryName, string datasetName)
        {
            try
            {
                sportshubTimestamp = await objAdf.RetrieveTimestamp(datafactoryName, datasetName);
                if (sportshubTimestamp != string.Empty)
                {
                    _specFlowOutputHelper.WriteLine("timestamp: {0}", sportshubTimestamp);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }

        }

        [Then(@"I validate dataset data is equal to ""(.*)""")]
        public void ThenIValidateDatasetDataIsEqualTo(string today)
        {
            bool isSameDates = false;
            try
            {
                isSameDates = objAdf.CompareDates(today);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(isSameDates, Is.True);
        }

        [Then(@"I get log message ""(.*)"" for pipeline ""(.*)""")]
        public async Task ThenIGetLogMessageForPipeline(string pipelineMessgae, string pipelineName)
        {
            try
            {
                await objAdf.GetPipelineLogMessage(pipelineMessgae, pipelineName);
                _specFlowOutputHelper.WriteLine("Pipeline Name: {0}", pipelineName);
                _specFlowOutputHelper.WriteLine("Pipeline Message: {0}", pipelineMessgae);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }

        }

        [Then(@"I validate ""(.*)""")]
        public async Task ThenIValidate(string log_message)
        {
            bool isLogValid = false;
            try
            {
                isLogValid = await objAdf.ValidateLogMessage();
                //await objAdf.ValidateLogMessage();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(isLogValid, Is.True);
        }

        [Then(@"I read file ""(.*)""")]
        public void ThenIReadFile(string filename)
        {
            try
            {
                LandingZonefileName = filename;
                if (filename.Contains(".csv"))
                {
                    objBlob.Readfile(filename);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
        }

        [When(@"I connect to result container and validate folder creation")]
        public void WhenIConnectToResultContainerAndValidateFolderCreation()
        {
            try
            {
                //Get Prefixes from file
                List<string> Prefixes = new List<string>();
                if (LandingZonefileName.Contains(".csv"))
                {
                    Prefixes = _helper.getPrefixListFromCSV(LandingZonefileName);
                }
                else if (LandingZonefileName.Contains(".json"))
                {
                    Prefixes = _helper.getPrefixListFromJSON(LandingZonefileName);
                }
                //Validate folder exists in azure directory
                bool isFolderExists = objBlob.ValidateFolders(Prefixes);
                _specFlowOutputHelper.WriteLine("Prefix list is:");
                for (int i=0;i<Prefixes.Count;i++)
                {
                    _specFlowOutputHelper.WriteLine(Prefixes[i]);
                }
                
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
                    
        }

        [Then(@"I have connected to database ""(.*)"" and fetch data for ""(.*)""")]
        public void ThenIHaveConnectedToDatabaseAndFetchDataFor(string databaseName, string tableName)
        {
            try
            {
                //Clearing all records
                if (dtFanInteractionIds != null)
                {
                    if (dtFanInteractionIds.Rows.Count > 0)
                    {
                        dtFanInteractionIds.Clear();
                    }
                }
                dtFanInteractionIds = objSql.GetData(databaseName, tableName);
                _specFlowOutputHelper.WriteLine("FanInteractionIDs (Count="+ dtFanInteractionIds.Rows.Count
                    +") from Database: " + Environment.NewLine 
                    +_helper.getFanInteractionIdsFromDB(dtFanInteractionIds));
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }

        }
        [Then(@"I validate ""(.*)"" from table is to equal ""(.*)"" from ProcessedFile")]
        public void ThenIValidateFromTableIsToEqualFromProcessedFile(string tableColName, string fileColName)
        {
            bool check = false;
            try
            {
                List<string> Prefixes = _helper.getPrefixListFromCSV(LandingZonefileName);
                string[] FanInteractionIDs = objBlob.ReadFanInteractionIDsFromParquet(Prefixes);
                check = _helper.ValidateColumn(FanInteractionIDs, dtFanInteractionIds);
                //Printing fan interaction ids in report
                _specFlowOutputHelper.WriteLine("Below are the List of FanInteractionIDs (Out of "+ FanInteractionIDs.Count()
                    + " Distinct Fan Interaction IDs are " + FanInteractionIDs.Distinct().Count()+") from Parquet files: " + Environment.NewLine +
                    _helper.getFanInteractionIDs(FanInteractionIDs));
                
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);

            }
            Assert.That(check, Is.True);


        }

        [Given(@"I have connected to database ""(.*)"" and fetch all columns for ""(.*)""")]
        public void GivenIHaveConnectedToDatabaseAndFetchAllColumnsFor(string databaseName, string tableName)
        {
            try
            {
                colList.Clear();
                colList = objSql.getColumnNames(databaseName,tableName);
                _specFlowOutputHelper.WriteLine(_helper.getColumnNames(colList) + Environment.NewLine);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
        }
        [Given(@"I validate column ""(.*)"" is present in database table")]
        public void GivenIValidateColumnIsPresentInDatabaseTable(string colName)
        {
            bool isColumnExist = false;
            try
            {
               isColumnExist= _helper.CheckItem(colList, colName);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                _specFlowOutputHelper.WriteLine("Error Message: " + ex.Message);
            }
            Assert.That(isColumnExist, Is.True);
        }


        [Scope(Feature = "ETL_ADF_EndToEnd")]
        [AfterFeature]
        public static void SignUpAF()
        {
            AfterFeatures.GenerateTestReport();
            if (dtFanInteractionIds != null)
            {
                AfterFeatures.DeleteTestData(dtFanInteractionIds);
            }
        }

    }
}
